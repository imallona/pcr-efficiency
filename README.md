## How to access PCR efficiency?

- In short: https://imallona.bitbucket.io/efficiency.html
- Long: we used to host the tool at the Technical Univ. Cartagena, but we are currently setting up a clould-based hosting elsewhere. So it is temporarily hosted it at Uni Zurich (thanks!); will keep linked the service at https://imallona.bitbucket.org/efficiency.html and http://srvgen.upct.es/efficiency.html

## What's PCR efficiency for?

Relative calculation of differential gene expression in quantitative PCR reactions requires comparison between amplification experiments that include reference genes and genes under study. Ignoring the differences between their efficiencies may lead to miscalculation of gene expression even with the same starting amount of template. Although there are several tools performing PCR primer design, there is no tool available that predicts PCR efficiency for a given amplicon and primer pair. 

We have used a statistical approach based on 90 primer pair combinations amplifying templates from bacteria, yeast, plants and humans, ranging in size between 74 and 907 bp to identify the parameters that affect PCR efficiency. We developed a generalized additive model fitting the data and constructed an open source Web interface that allows the obtention of oligonucleotides optimized for PCR with predicted amplification efficiencies starting from a given sequence. 

pcrEfficiency provides an easy-to-use web interface predicting PCR efficiencies prior to web lab experiments thus easing quantitative real-time PCR set-up. A web-based service as well the source code are provided freely at efficiency.html under the GPL v2 license.

### Publication

To know more about the method please check the manuscript _pcrEfficiency: a Web tool for PCR amplification efficiency prediction_ published in BMC Bioinformatics 2011, 12:404 at https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-404

## Deployment

We released our tool in 2009 but (re)deployed the service in 2019 using modern Biopython, EMBOSS and Primer3 to benefit from their beautiful latests developments. Not surprisingly, dependencies were not fully back-compatible, so we adapted our CGI python scripts to fit to them.

If interested in deploying pcr-efficiency yourself, please note that, due to the modularity of the primer3 suite, system set-ups is a bit convoluted. We mainly used standard debian `apt`-based package managers (see below).

### Primer3 (compiled)

Needed to sustain the `EMBOSS_PRIMER32_CORE=/usr/bin/primer3_core` export.

```
mkdir -p ~/soft
cd $_

sudo apt-get install -y build-essential g++ cmake git-all
git clone https://github.com/primer3-org/primer3.git primer3

cd primer3/src

make
make test

sudo cp primer3_core /usr/bin/primer3_core
```

### BioPython

```
sudo apt-get install python-biopython
```

### EMBOSS and primer3

```
sudo apt-get install emboss primer3
```

### Envvars

Importantly, `EMBOSS_PRIMER32_CORE=/usr/bin/primer3_core` must be exported for `eprimer3` to run. Please note the `SetEnv` apache directives (we attach relevant apache confs at `deployment/apache_conf`.


## Repository folders

- `modelling`, the scripts and data to reproduce the paper
- `deployment/apache_conf`, relevant apache set-up to export the environmental variable `EMBOSS_PRIMER32_CORE` (some `SetEnvs` are redundant)
- `deployment/cgi-bin`, where `testing.py` is the script used in production.
- `deployment/html`, the static content


## Contact

Izaskun Mallona (izaskun.mallona at gmail.com, http://imallona.bitbucket.io) or Marcos Egea (marcos.egea at upct.es, http://upct.es/~genetica)

## Last updated

Wed May 15 10:00:47 CEST 2019
