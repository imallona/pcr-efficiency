#!/usr/bin/env R
# -*- coding: utf-8  -*-

# This file is part of PCR efficiency calculator.  
# PCR efficiency calculator is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright Izaskun Mallona
# izaskun.mallona@gmail.com

library(coin)
library(compute.es)

# template start


## GD vs CD
wilcox_test(efficiency~factor(template), data=dataMini[dataMini$template=='CD'|dataMini$template=='GD',])
# Z = -31.1635, p-value < 2.2e-16

pes(2.2e-16, nrow(dataMini[dataMini$template=='CD',]),nrow(dataMini[dataMini$template=='GD',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.271137920 0.001090677 0.271085084 0.001090252 

#$Correlation
#           r        var.r 
#0.1317235345 0.0002485645 

#$Log_Odds
#    log_odds var.log_odds 
# 0.491789787  0.003588183 

#$Fishers_z
#           z        var.z 
#0.1324934144 0.0002598753 

#$TotalSample
#   n 
#3851 


## CD vs plasmid
wilcox_test(efficiency~factor(template), data=dataMini[dataMini$template=='CD'|dataMini$template=='plasmid',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(template) (CD, plasmid) 
#Z = -12.0695, p-value < 2.2e-16
#alternative hypothesis: true mu is not equal to 0 

pes(2.2e-16, nrow(dataMini[dataMini$template=='CD',]),nrow(dataMini[dataMini$template=='plasmid',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.559512222 0.004644272 0.559347385 0.004641536 

#$Correlation
#           r        var.r 
#0.1615837128 0.0003673788 

#$Log_Odds
#    log_odds var.log_odds 
#  1.01484291   0.01527904 

#$Fishers_z
#           z        var.z 
#0.1630124414 0.0003929273 

#$TotalSample
#   n 
#2548 

## GD vs plasmid
wilcox_test(efficiency~factor(template), data=dataMini[dataMini$template=='GD'|dataMini$template=='plasmid',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(template) (GD, plasmid) 
#Z = -9.9228, p-value < 2.2e-16
#alternative hypothesis: true mu is not equal to 0 


pes(2.2e-16, nrow(dataMini[dataMini$template=='GD',]),nrow(dataMini[dataMini$template=='plasmid',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.574095802 0.004889367 0.573854281 0.004885254 

#$Correlation
#           r        var.r 
#0.1925205081 0.0005098385 

#$Log_Odds
#    log_odds var.log_odds 
#  1.04129460   0.01608537 

#$Fishers_z
#           z        var.z 
#0.1949533812 0.0005611672 

#$TotalSample
#   n 
#1785 


## yGD vs plasmid
wilcox_test(efficiency~factor(template), data=dataMini[dataMini$template=='yGD'|dataMini$template=='plasmid',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(template) (plasmid, yGD) 
#Z = 8.8896, p-value < 2.2e-16
#alternative hypothesis: true mu is not equal to 0 

pes(2.2e-16, nrow(dataMini[dataMini$template=='yGD',]),nrow(dataMini[dataMini$template=='plasmid',]))

#          d       var.d           g       var.g 
#0.777071206 0.008962478 0.775851314 0.008934360 

#$Correlation
#          r       var.r 
#0.362157469 0.001469545 

#$Log_Odds
#    log_odds var.log_odds 
#  1.40945126   0.02948537 

#$Fishers_z
#          z       var.z 
#0.379366830 0.002096436 

#$TotalSample
#  n 
# 480 

## yGD vs CD
wilcox_test(efficiency~factor(template), data=dataMini[dataMini$template=='yGD'|dataMini$template=='CD',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(template) (CD, yGD) 
#Z = 3.9596, p-value = 7.506e-05
#alternative hypothesis: true mu is not equal to 0 

pes(7.506e-05, nrow(dataMini[dataMini$template=='yGD',]),nrow(dataMini[dataMini$template=='CD',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.269510332 0.004631828 0.269430870 0.004629098 

#$Correlation
#          r       var.r 
#0.078361414 0.000386773 

#$Log_Odds
#    log_odds var.log_odds 
#   0.4888377    0.0152381 

#$Fishers_z
#           z        var.z 
#0.0785224003 0.0003932363 

#$TotalSample
#   n 
#2546 

## yGD vs GD
wilcox_test(efficiency~factor(template), data=dataMini[dataMini$template=='yGD'|dataMini$template=='GD',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(template) (GD, yGD) 
#Z = 9.7282, p-value < 2.2e-16
#alternative hypothesis: true mu is not equal to 0 

pes(2.2e-16, nrow(dataMini[dataMini$template=='yGD',]),nrow(dataMini[dataMini$template=='GD',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.576176063 0.004924864 0.575933395 0.004920717 

#$Correlation
#           r        var.r 
#0.1926264601 0.0005103565 

#$Log_Odds
#    log_odds var.log_odds 
#  1.04506778   0.01620215 

#$Fishers_z
#           z        var.z 
#0.1950634137 0.0005617978 

#$TotalSample
#   n 
#1783 

# template analysis end

# 3' termini analysis start

spam<-rbind(cbind(dataMini$efficiency, dataMini$trap3Rev),cbind(dataMini$efficiency,dataMini$trap3For))

nudge<- na.omit(spam)
colnames(nudge) <- c('efficiency','terminus')
spam<-as.data.frame(nudge)
spam$efficiency<-as.numeric(spam$efficiency)

# purines will be 'U' and pyrimidines 'Y'
gsub('[CG]','U',gsub('[AT]','Y',spam$terminus))->spam$chem

wilcox_test(efficiency~factor(chem), data=spam[spam$chem=='UU'|spam$chem=='YY',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(chem) (UU, YY) 
#Z = -8.3872, p-value < 2.2e-16
#alternative hypothesis: true mu is not equal to 0 

pes(2.2e-16, nrow(spam[spam$chem=='UU',]),nrow(spam[spam$chem=='YY',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.275348845 0.001124811 0.275291444 0.001124342 

#$Correlation
#           r        var.r 
#0.1361974158 0.0002650863 

#$Log_Odds
#    log_odds var.log_odds 
# 0.499427559  0.003700479 

#$Fishers_z
#           z        var.z 
#0.1370490567 0.0002780095 

#$TotalSample
#   n 
3600 

## UU vs UY

wilcox_test(efficiency~factor(chem), data=spam[spam$chem=='UU'|spam$chem=='UY',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(chem) (UU, UY) 
#Z = -6.2664, p-value = 3.695e-10
#alternative hypothesis: true mu is not equal to 0 

pes(3.695e-10, nrow(spam[spam$chem=='UU',]),nrow(spam[spam$chem=='UY',]))

#$MeanDifference
#           d        var.d            g        var.g 
#0.1977330262 0.0009955728 0.1976975202 0.0009952153 

#$Correlation
#           r        var.r 
#0.0967138293 0.0002337379 

#$Log_Odds
#    log_odds var.log_odds 
# 0.358648037  0.003275303 

#$Fishers_z
#           z        var.z 
#0.0970170727 0.0002394636 

#$TotalSample
#   n 
#4179 

## UU vs YU

wilcox_test(efficiency~factor(chem), data=spam[spam$chem=='UU'|spam$chem=='YU',])


#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(chem) (UU, YU) 
#Z = -8.741, p-value < 2.2e-16
#alternative hypothesis: true mu is not equal to 0 

pes(2.2e-16, nrow(spam[spam$chem=='UU',]),nrow(spam[spam$chem=='YU',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.278347520 0.001149441 0.278288124 0.001148951 

#$Correlation
#           r        var.r 
#0.1377802172 0.0002710432 

#$Log_Odds
#    log_odds var.log_odds 
#  0.50486655   0.00378151 

#$Fishers_z
#          z       var.z 
#0.138662129 0.000284576 

#$TotalSample
#   n 
#3517 

## UY vs YU

wilcox_test(efficiency~factor(chem), data=spam[spam$chem=='UY'|spam$chem=='YU',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(chem) (UY, YU) 
#Z = -3.1566, p-value = 0.001596
#alternative hypothesis: true mu is not equal to 0 

pes( 0.001596, nrow(spam[spam$chem=='UY',]),nrow(spam[spam$chem=='YU',]))

#$MeanDifference
#           d        var.d            g        var.g 
#0.0976434970 0.0009567241 0.0976264096 0.0009563893 

#$Correlation
#           r        var.r 
#0.0481803989 0.0002318581 

#$Log_Odds
#    log_odds var.log_odds 
# 0.177105713  0.003147496 

#$Fishers_z
#           z        var.z 
#0.0482177321 0.0002333722 

#$TotalSample
#   n 
#4288 

# UY vs YY

wilcox_test(efficiency~factor(chem), data=spam[spam$chem=='UY'|spam$chem=='YY',])
#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(chem) (UY, YY) 
#Z = -2.2721, p-value = 0.02308
#alternative hypothesis: true mu is not equal to 0 

pes(0.02308, nrow(spam[spam$chem=='UY',]),nrow(spam[spam$chem=='YY',]))

#$MeanDifference
#          d       var.d           g       var.g 
#0.069369048 0.000932017 0.069357139 0.000931697 

#$Correlation
#          r       var.r 
#0.034358580 0.000228106 

#$Log_Odds
#    log_odds var.log_odds 
# 0.125821536  0.003066213 

#$Fishers_z
#           z        var.z 
#0.0343721094 0.0002289377 

#$TotalSample
#   n 
#4371 

## YU vs UY

wilcox_test(efficiency~factor(chem), data=spam[spam$chem=='YU'|spam$chem=='UY',])

#	Asymptotic Wilcoxon Mann-Whitney Rank Sum Test

#data:  efficiency by factor(chem) (UY, YU) 
#Z = -3.1566, p-value = 0.001596
#alternative hypothesis: true mu is not equal to 0 

pes(0.001596, nrow(spam[spam$chem=='YU',]),nrow(spam[spam$chem=='UY',]))

#$MeanDifference
#           d        var.d            g        var.g 
#0.0976434970 0.0009567241 0.0976264096 0.0009563893 

#$Correlation
#           r        var.r 
#0.0481803989 0.0002318581 

#$Log_Odds
#    log_odds var.log_odds 
# 0.177105713  0.003147496 

#$Fishers_z
#           z        var.z 
#0.0482177321 0.0002333722 

#$TotalSample
#   n 
#4288 



# 3' termini analysis end
