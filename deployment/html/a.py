from Bio import SeqIO
from Bio.Blast import NCBIStandalone
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML

EXPECT = 1e-10

print "Opening file..."
query_file = open("input.fasta")
result_file = "blast.xml"


handle = open("input.fasta", "rU")
print "Parsing with SeqIO"
records = list(SeqIO.parse(handle, "fasta"))
handle.close()

i=0
print "Done, proceed with blast"

for i in range(len(records)):
    print "Sequence number %s was sent to NCBI" %i
    
    result_handle = NCBIWWW.qblast("blastn", "nr", records[i].seq.tostring(), expect=EXPECT, descriptions=1, alignments=1,hitlist_size=1, format_type="text")
    resultFileName="blast_"+records[i].id+".html"
    
    print "Writting results in file named %s" %(resultFileName)
    result=open(resultFileName,'w')
    result.write(result_handle.read())
    result.close()
    
print "Success. Finishing."
