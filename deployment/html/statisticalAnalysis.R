#PCR efficiency script v2.2
#Model build-up

#Copyright 2010 Izaskun Mallona
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#Data structure example
#species	name	sequence	forlength	revlength	source	efficiency	template	operator
#S.c.	ITS	CCGGTAAGGGTGGACCTGCAATAAGAAGAGGG	19	20	YPD	1.72	GD	Julia
#S.c.	ITS	CCGTAGGTGAACTATCAATAAGCGGAGGA	19	20	YPD	1.66	GD	Julia

#libraries installation
#install.packages("coin","multcompView","R2HTML", "MASS")

#bioconductor installation
#source("http://bioconductor.org/biocLite.R")
#biocLite()

#libraries loading
library(coin)
library(Biostrings)
library(multcompView)
library(R2HTML)
#library(lme4)
library(MASS)

#data import

read.table("data.csv",header=T)->data

data[data$efficiency<2 & data$efficiency>1,] -> data

data$sequence -> sequence

sequence <- DNAStringSet(as.vector(sequence))

#length computing

width(sequence) -> data$lengthSequence

data$primersLength <- data$forLength+data$revLength

#forward primer

substring(sequence, 1, data$forLength)->data$forward

forward <- DNAStringSet(as.vector(data$forward))

#reverse primer

substring(data$sequence, (data$lengthSequence-data$revLength), data$length)-> data$reverseRevcom

reverseRevcom <- DNAStringSet(data$reverseRevcom)
reverseComplement(reverseRevcom)->reverse

data$reverse <- as.character(reverse)

#GC and base calculus with Biostrings
alphSequence <- alphabetFrequency(sequence, baseOnly=TRUE)
data$gcSequence <- rowSums(alphSequence[,c("G", "C")]) / rowSums(alphSequence)

alphForward <- alphabetFrequency(forward, baseOnly=TRUE)
gcForward <- rowSums(alphForward[,c("G", "C")]) / rowSums(alphForward) 

alphReverse <- alphabetFrequency(reverse, baseOnly=TRUE)
gcReverse <- rowSums(alphReverse[,c("G", "C")]) / rowSums(alphReverse) 

data$gcPrimers<- (gcForward+gcReverse)/2

#G+C content differences between primers

data$gcImbalance <- abs(gcForward-gcReverse)

#repeats presence

aRepeats<-vector()
for (i in 1:(length(sequence))) {
	c(aRepeats,seq(along=sequence[i]) %in% grep("AAAAAA",sequence[i]))->aRepeats
}

tRepeats<-vector()
for (i in 1:(length(data$sequence))) {
	c(tRepeats,seq(along=sequence[i]) %in% grep("TTTTTT",sequence[i]))->tRepeats
}

cRepeats<-vector()
for (i in 1:(length(sequence))) {
	c(cRepeats,seq(along=sequence[i]) %in% grep("CCCCCC",sequence[i]))->cRepeats
}

gRepeats<-vector()
for (i in 1:(length(sequence))) {
	c(gRepeats,seq(along=sequence[i]) %in% grep("GGGGGG",sequence[i]))->gRepeats
}

cbind(data,aRepeats,tRepeats,cRepeats,gRepeats)->data
rm(aRepeats,tRepeats,cRepeats,gRepeats)

#repeat counter

aFreq<-as.vector(unlist(lapply(gregexpr("A{4,}",sequence),length)))
aPresence<-ifelse(regexpr("A{4,}",sequence)<0,0,1)
	aFreq*aPresence->aCount

tFreq<-as.vector(unlist(lapply(gregexpr("T{4,}",sequence),length)))
tPresence<-ifelse(regexpr("T{4,}",sequence)<0,0,1)
	tFreq*tPresence->tCount

cFreq<-as.vector(unlist(lapply(gregexpr("C{4,}",sequence),length)))
cPresence<-ifelse(regexpr("C{4,}",sequence)<0,0,1)
	cFreq*cPresence->cCount

gFreq<-as.vector(unlist(lapply(gregexpr("G{4,}",sequence),length)))
gPresence<-ifelse(regexpr("G{4,}",sequence)<0,0,1)
	gFreq*gPresence->gCount

aCount+tCount->data$pyrimidines
gCount+cCount->data$purines

#melting temperature. Marmur,J., and Doty,P. (1962) J Mol Biol 5:109-118
#
#2*(alphForward[,1]+alphForward[,4])+4*(alphForward[,2]+alphForward[,3])->data$tmForward
#2*(alphReverse[,1]+alphReverse[,4])+4*(alphReverse[,2]+alphReverse[,3])->data$tmReverse

#Tm= 100.5 + (41 * (yG+zC)/(wA+xT+yG+zC)) - (820/(wA+xT+yG+zC)) + 16.6*log10([Na+])
#
#Howley,P.M., Israel,M.F., Law,M-F., and Martin,M.A. (1979) J Biol Chem 254:4876-4883
#data$tmForward <- 100.5 + (41 * (alphForward[,3]+alphForward[,2])/(alphForward[,1]+alphForward[,4]+alphForward[,3]+alphForward[,2])) - (820/(alphForward[,1]+alphForward[,4]+alphForward[,3]+alphForward[,2])) + 16.6*log10(0.40)

#melting temperature. Wallace,R.B., Shaffer,J., Murphy,R.F., Bonner,J., Hirose,T., and Itakura,K. (1979) Nucleic Acids Res 6:3543-3557 (Abstract) and Sambrook,J., and Russell,D.W. (2001) Molecular Cloning: A Laboratory Manual. Cold Spring Harbor Laboratory Press; Cold Spring Harbor, NY

data$tmForward <- 64.9 +41*(alphForward[,3]+alphForward[,2]-16.4)/(alphForward[,1]+alphForward[,4]+alphForward[,3]+alphForward[,2])

data$tmReverse <- 64.9 +41*(alphReverse[,3]+alphReverse[,2]-16.4)/(alphReverse[,1]+alphReverse[,4]+alphReverse[,3]+alphReverse[,2])

data$tmPrimers <- (data$tmForward+data$tmReverse)/2

#3' traps

substring(data$forward, data$forLength-1, data$forLength) -> data$trap3For

substring(data$reverse, data$revLength-1, data$revLength) -> data$trap3Rev

substring(data$forward,data$forLength,data$forLength)->data$trap3LastFor

substring(data$reverse,data$revLength,data$revLength)->data$trap3LastRev

#triplet extraction (sliding window)

k<- vector()
forwardTriplets <- vector()
for (i in 1:length(data$forward)){
	substring(data$forward[i], 1:nchar(data$forward[i]), 1:nchar(data$forward[i])+2)-> k
	cbind(forwardTriplets,k)->forwardTriplets
}
rm(k)

k<- vector()
reverseTriplets <- vector()
for (i in 1:length(data$reverse)){
	substring(data$reverse[i], 1:nchar(data$reverse[i]), 1:nchar(data$reverse[i])+2)-> k
	cbind(reverseTriplets,k)->reverseTriplets
}
rm(k)

# complementary strand generation and triplet extraction: forward primer

reverseComplement(forward) -> forwardRevcom

data$forwardRevcom <- as.character(forwardRevcom)

k<- vector()
forwardRevcomTriplets <- vector()
for (i in 1:length(data$forwardRevcom)){
	substring(data$forwardRevcom[i], 1:nchar(data$forwardRevcom[i]), 1:nchar(data$forward[i])+2)-> k
	cbind(forwardRevcomTriplets,k)->forwardRevcomTriplets
}
rm(k)

# the same with reverse primer

data$reverseRevcom <- as.character(reverseRevcom)

k<- vector()
reverseRevcomTriplets<-vector()
for (i in 1:length(data$reverseRevcom)){
	substring(data$reverseRevcom[i], 1:nchar(data$reverseRevcom[i]), 1:nchar(data$reverseRevcom[i])+2)-> k
	cbind(reverseRevcomTriplets,k)->reverseRevcomTriplets
}
rm(k)

#selfcomplementarity forward

for (i in 1:length(data$forward)) {
 	sum(!is.na(match(forwardRevcomTriplets[,i],forwardTriplets[,i]))) -> data$forwardSelfcom[i]
}

#selfcomplementarity reverse

for (i in 1:length(data$reverse)) {
 	sum(!is.na(match(reverseRevcomTriplets[,i],reverseTriplets[,i]))) -> data$reverseSelfcom[i]
}

data$primersSelfcom <- (data$forwardSelfcom+data$reverseSelfcom)

#primer-dimer check

for (i in 1:length(data$reverse)) {
	sum(!is.na(match(forwardTriplets[,i],reverseRevcomTriplets[,i]))) -> data$primerDimers[i]
}

#biostrings palindrome search
data$sequencePalindromes<-vector()

for (i in 1:length(sequence)) {
	length(findComplementedPalindromes(DNAString(sequence[[i]])))->data$sequencePalindromes[i]
}


pdf(file="efficiency.pdf")
	hist(data$efficiency)
	qqnorm(data$efficiency)
dev.off()

shapiro.test(data$efficiency)

#data output

write.csv(data,"processed_data.csv")

#data analysis
HTML(spearman_test(data$efficiency~data$primersLength),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$lengthSequence),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$gcSequence),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$pyrimidines),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$purines),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$tmPrimers),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$primersSelfcom),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$primerDimers),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$gcImbalance),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$gcPrimers),file="coin_results.html")
HTML(spearman_test(data$efficiency~data$sequencePalindromes),file="coin_results.html")

HTML(kruskal_test(data$efficiency~data$species),file="coin_results.html")
HTML(kruskal_test(data$efficiency~data$template),file="coin_results.html")
HTML(kruskal_test(data$efficiency~data$var),file="coin_results.html")
HTML(kruskal_test(data$efficiency~data$source),file="coin_results.html")
HTML(kruskal_test(data$efficiency~data$operator),file="coin_results.html")
HTML(kruskal_test(data$efficiency~as.factor(data$trap3For)),file="coin_results.html")
HTML(kruskal_test(data$efficiency~as.factor(data$trap3Rev)),file="coin_results.html")
HTML(kruskal_test(data$efficiency~as.factor(data$trap3LastFor)),file="coin_results.html")
HTML(kruskal_test(data$efficiency~as.factor(data$trap3LastRev)),file="coin_results.html")

##forward
#pairwise.wilcox.test(data$efficiency,data$trap3For,p.adj="bonf") -> pwt_for
#write.table(pwt_for$p.value, file="pwt_for.txt")
#	#add diagonal mannually, otherwise "AA" dinucleotide will not be taken into account!
#read.table("pwt_for_checked.txt")->pwt_for
#HTML(multcompLetters(pwt_for), file="multcompLetters.html")

##reverse
#pairwise.wilcox.test(data$efficiency,data$trap3Rev,p.adj="bonf") -> pwt_rev
#write.table(pwt_rev$p.value, file="pwt_rev.txt")
#	#add diagonal mannually, otherwise "AA" dinucleotide will not be taken into account!
#read.table("pwt_rev_checked.txt")->pwt_rev
#HTML(multcompLetters(pwt_rev), file="multcompLetters.html")

#both primers
HTML(kruskal_test(as.numeric(rep(data$efficiency,2))~as.factor(c(data$trap3For,data$trap3Rev))), file ="coin_results.html")
pairwise.wilcox.test(as.numeric(rep(data$efficiency,2)),as.factor(c(data$trap3For,data$trap3Rev)), p.adj="bonf") -> pwt
write.table(pwt$p.value, file="pwt.txt")
	#add diagonal mannually, otherwise "AA" dinucleotide will not be taken into account!
read.table("pwt_checked.txt")->pwt
HTML(multcompLetters(pwt), file="coin_results.html")

# boxplots
pdf("boxplots.pdf")
	boxplot(as.numeric(rep(data$efficiency,2))~as.factor(c(data$trap3For,data$trap3Rev)),las=2,ylab="Efficiency", xlab="3' tail")
	boxplot(data$efficiency~data$primersLength,las=2,ylab="Efficiency", xlab="Primers length")
	boxplot(data$efficiency~data$lengthSequence,las=2,ylab="Efficiency", xlab="Sequence length")
	boxplot(data$efficiency~data$gcSequence,las=2,ylab="Efficiency", xlab="Sequence GC content",names=round(sort(unique(data$gcSequence)),2))
	boxplot(data$efficiency~data$pyrimidines,las=2,ylab="Efficiency", xlab="Pyrimidine repeats")
	boxplot(data$efficiency~data$purines,las=2,ylab="Efficiency", xlab="Purine repeats")
	boxplot(data$efficiency~data$tmPrimers,las=2,ylab="Efficiency", xlab="Primers melting temperature",names=round(sort(unique(data$tmPrimers)),2))
	boxplot(data$efficiency~data$primersSelfcom,las=2,ylab="Efficiency", xlab="Primers selfcomplementarity")
	boxplot(data$efficiency~data$primerDimers,las=2,ylab="Efficiency", xlab="Primer dimers")
	boxplot(data$efficiency~data$gcImbalance,las=2,ylab="Efficiency", xlab="GC content imbalance between primers", names=round(sort(unique(data$gcImbalance)),2))
	boxplot(data$efficiency~data$sequencePalindromes,las=2,ylab="Efficiency", xlab="Sequence palyndromes")
	boxplot(data$efficiency~data$species,las=2,ylab="Efficiency", xlab="Species")
	boxplot(data$efficiency~data$template,las=2,ylab="Efficiency", xlab="Template")
	boxplot(data$efficiency~data$var,las=2,ylab="Efficiency", xlab="Variety or line")
	boxplot(data$efficiency~data$source,las=2,ylab="Efficiency", xlab="Source of DNA")
	boxplot(data$efficiency~data$operator,las=2,ylab="Efficiency", xlab="Operator")
dev.off()

#Kruskal tests (CD vs GD)
HTML(kruskal_test(
c(
	data$efficiency[data$template=="GN"&data$name=="PhTubulin"], 
	data$efficiency[data$template=="CD"&data$name=="PhTubulin"])
~
as.factor(c(
	data$template[data$template=="GN"&data$name=="PhTubulin"], 
	data$template[data$template=="CD"&data$name=="PhTubulin"]))
), file= "CDvsGN.html")

#with dTph, too
a1<-c(
	data$efficiency[data$template=="GN"&data$name=="PhTubulin"], 
	data$efficiency[data$template=="CD"&data$name=="PhTubulin"],
	data$efficiency[data$template=="GN"&data$name=="PhDtph"],
	data$efficiency[data$template=="CD"&data$name=="PhDtph"])
a2<-as.factor(c(
	data$template[data$template=="GN"&data$name=="PhTubulin"], 
	data$template[data$template=="CD"&data$name=="PhTubulin"],
	data$template[data$template=="GN"&data$name=="PhDtph"],
	data$template[data$template=="CD"&data$name=="PhDtph"]))

HTML(kruskal_test(a1~a2),file ="CDvsGN.html") 

#model fitting

lmerFinal<- lmer(efficiency ~ primerDimers + gcImbalance + gcSequence + primersSelfcom + (1 | operator/species/var/template/source), REML=F, data=unido)
summary(lmerFinal)
lmerFinal.fitted<- fitted(lmerFinal)
