#efficiencyValidation.R v1
#validation of pcrEfficiency model

#Copyright 2010 Izaskun Mallona
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#libraries loading
library(coin)
library(Biostrings)
library(multcompView)
library(R2HTML)
#library(lme4)
library(MASS)

#data import

read.csv("data6thMay.csv",header=T)->data

data$sequence -> sequence

sequence <- DNAStringSet(as.vector(sequence))

#length computing

width(sequence) -> data$lengthSequence

data$primersLength <- data$forLength+data$revLength

#forward primer

substring(sequence, 1, data$forLength)->data$forward

forward <- DNAStringSet(as.vector(data$forward))

#reverse primer

substring(data$sequence, (data$lengthSequence-data$revLength), data$length)-> data$reverseRevcom

reverseRevcom <- DNAStringSet(data$reverseRevcom)
reverseComplement(reverseRevcom)->reverse

data$reverse <- as.character(reverse)

#GC and base calculus with Biostrings
alphSequence <- alphabetFrequency(sequence, baseOnly=TRUE)
data$gcSequence <- rowSums(alphSequence[,c("G", "C")]) / rowSums(alphSequence)

alphForward <- alphabetFrequency(forward, baseOnly=TRUE)
gcForward <- rowSums(alphForward[,c("G", "C")]) / rowSums(alphForward) 

alphReverse <- alphabetFrequency(reverse, baseOnly=TRUE)
gcReverse <- rowSums(alphReverse[,c("G", "C")]) / rowSums(alphReverse) 

data$gcPrimers<- (gcForward+gcReverse)/2

#G+C content differences between primers

data$gcImbalance <- abs(gcForward-gcReverse)


#triplet extraction (sliding window)

k<- vector()
forwardTriplets <- vector()
for (i in 1:length(data$forward)){
	substring(data$forward[i], 1:nchar(data$forward[i]), 1:nchar(data$forward[i])+2)-> k
	cbind(forwardTriplets,k)->forwardTriplets
}
rm(k)

k<- vector()
reverseTriplets <- vector()
for (i in 1:length(data$reverse)){
	substring(data$reverse[i], 1:nchar(data$reverse[i]), 1:nchar(data$reverse[i])+2)-> k
	cbind(reverseTriplets,k)->reverseTriplets
}
rm(k)

# complementary strand generation and triplet extraction: forward primer

reverseComplement(forward) -> forwardRevcom

data$forwardRevcom <- as.character(forwardRevcom)

k<- vector()
forwardRevcomTriplets <- vector()
for (i in 1:length(data$forwardRevcom)){
	substring(data$forwardRevcom[i], 1:nchar(data$forwardRevcom[i]), 1:nchar(data$forward[i])+2)-> k
	cbind(forwardRevcomTriplets,k)->forwardRevcomTriplets
}
rm(k)

# the same with reverse primer

data$reverseRevcom <- as.character(reverseRevcom)

k<- vector()
reverseRevcomTriplets<-vector()
for (i in 1:length(data$reverseRevcom)){
	substring(data$reverseRevcom[i], 1:nchar(data$reverseRevcom[i]), 1:nchar(data$reverseRevcom[i])+2)-> k
	cbind(reverseRevcomTriplets,k)->reverseRevcomTriplets
}
rm(k)

#selfcomplementarity forward

for (i in 1:length(data$forward)) {
 	sum(!is.na(match(forwardRevcomTriplets[,i],forwardTriplets[,i]))) -> data$forwardSelfcom[i]
}

#selfcomplementarity reverse

for (i in 1:length(data$reverse)) {
 	sum(!is.na(match(reverseRevcomTriplets[,i],reverseTriplets[,i]))) -> data$reverseSelfcom[i]
}

data$primersSelfcom <- (data$forwardSelfcom+data$reverseSelfcom)

#primer-dimer check

for (i in 1:length(data$reverse)) {
	sum(!is.na(match(forwardTriplets[,i],reverseRevcomTriplets[,i]))) -> data$primerDimers[i]
}


pcrEfficiency <- function(primerDimers, gcImbalance, gcSequence, primersSelfcom){
    predicted = (1.6953225 - 0.0069873 * primerDimers + 0.4949575 * gcImbalance + 0.0134166 *gcSequence  - 0.0006811 * primersSelfcom)
    return(predicted)
}

for ( i in 1:length(data$name) ){
data$estimated[i]<-pcrEfficiency(data$primerDimers[i], data$gcImbalance[i], data$gcSequence[i], data$primersSelfcom[i])
}

#roughly
HTML(cor(data$estimated,data$cpD2_efficiency),"validation.html")
HTML(cor(data$estimated,data$CQ_efficiency),"validation.html")

meanPredicted= vector()
meanPredictedName=vector
lev<-levels(data$name)
for (i in 1:length(lev)){
 meanPredicted= c(meanPredicted, mean(subset(data$estimated,data$name==lev[i])))
 }
 
 cqBased= vector()
lev<-levels(data$name)
for (i in 1:length(lev)){
 cqBased= c(cqBased, mean(subset(data$CQ_efficiency,data$name==lev[i])))
 }
 
 cpD2Based= vector()
lev<-levels(data$name)
for (i in 1:length(lev)){
 cpD2Based= c(cpD2Based, mean(subset(data$cpD2_efficiency,data$name==lev[i])))
 }
 
HTML(cor(meanPredicted,cqBased),"unique _validation.html")
HTML(cor(meanPredicted,cpD2Based),"unique_validation.html")
 
 #coin based
 
HTML(spearman_test(data$estimated~data$cpD2_efficiency),"unique_validation.html")
HTML(spearman_test(data$estimated~data$CQ_efficiency),"unique_validation.html")

HTML(spearman_test(meanPredicted~cqBased),"unique_validation.html")
HTML(spearman_test(meanPredicted~cpD2Based),"unique_validation.html")
