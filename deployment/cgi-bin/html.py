#!/usr/bin/env python
# -*- coding: utf-8  -*-

# This file is part of PCR efficiency calculator.  
# PCR efficiency calculator is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright Izaskun Mallona
# izaskun.mallona@gmail.com

import sys
import textwrap

class html:
    """CGI output handling."""
    
    def error(self):
        """Filling error message: two primers are required."""
        
        self.cssUp()
        print"""
        <div class="articles"> 
        Please fill at least the 'amplicon sequence' field in the form. This tool will estimate an efficiency value when two primers are provided, or, if both of them aren't, it will design some primers and thereafter an efficiency estimation will be provided for each suggested primer pair. Please go back to the <a href="../web.html">form</a>.
        </div> 
        """
        self.cssBottom()
        
        sys.exit()  

    def primerSelectionHeader(self,name):
        """HTML upper part of the result page."""
        
        print "<h2>Job name %s </h2><br /><br />" %name
        print "Primer pairs were designed according to the data you entered; for each one, efficiency value was estimated.<br/><br/>"
               
    def cssUp(self):
        """ CSS-driven HTML header."""
        
        print "Content-type: text/html\n"  
        print"""
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
         <title>PCR efficiency calculator Output</title>
         <meta http-equiv="Content-Language" content="English">
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta name="Description" content="PCR efficiency estimator; efficient primer design tool">
         <meta name="KEYWORDS" content="PCR, efficiency, amplification, primers, qPCR, Q-PCR, RT-PCR, primer3">
         <link rel="stylesheet" type="text/css" href="../style.css" media="screen">

        </head>

        <body>
        <div id="wrap">

        <div id="header">
         <table>
          <tbody>
           <tr>
            <td width="80%">
	         <h1><a href="efficiency.html">PCR efficiency Calculator</a></h1>

	         <h2>Primer- and amplicon-specific PCR estimation tool</h2>
            </td>
            </tr>
          </tbody>

        </table>
        </div>


        <div id="content"> 
        <div class="left">  
        <h2><a href="#">Tool description</a></h2> 
        <div class="articles"> 
        Primer Efficiency is an open source tool which allows PCR efficiency estimation. <a href="../documentation.html">Documentation</a> is available.
        <br /> 
        </div> 
        <h2><a href="#">Efficiency calculator output</a></h2>
        <div class="articles">"""

    def cssBottom(self): 
        """CSS-driven HTML bottom."""
        
        print """
        </div>
        </div>
        <div class="right"> 

        <h2>Categories :</h2>

        <ul>
        <li><a href="../license.html">License</a></li> 
        <li><a href="../documentation.html">Documentation</a></li>
        <li><a href="../examples.html">Examples</a></li> 


        <li><a href="http://www.upct.es/~genetica">Lab's main page</a></li>
        <li><a href="../Other.html">Other tools</a></li>
        <li><a href="http://imallona.bitbucket.io">Feedback</a></li>
        </ul>
        <table cellspacing="20" align="left">
          <tbody>
            <tr>
              <td><a href="http://www.upct.es/en/"><IMG src="../thumbnailUpct.png" height="35"  border="0" alt="UPCT web page"></a></td>
            </tr>
            <tr>

              <td><a href="http://www.f-seneca.org/seneca/html/inicio.htm"><IMG src="../thumbnailSeneca.png" height="45" border="0" alt="Fundación Séneca web page"></a></td>
            </tr>
            <tr>
              <td> <a href="http://www.micinn.es/portal/site/MICINN/?lang_choosen=en"><IMG src="../thumbnailMicinn.png" height="40" border="0" alt="MICINN web page"></a></td>
            </tr>
          </tbody>
        </table>

        </div>
        <div style="clear: both;"> </div>

        </div>

        </div>
        <div id="footer">
        Copyright 2010, 2019 Izaskun Mallona, Genetics, Technical University of Cartagena. <a href="http://validator.w3.org/check?uri=referer">W3C HTML 4.01 compliant</a>.
        </div>
        </body>
        </html>
        """
      
    def onePrimer(self):
        """Only one primer added warning."""
        
        self.cssUp()
        print """<div class="articles"> 
        You have entered one primer sequence only. This tool will estimate an efficiency value when two primers are provided, or, if both of them aren't, it will design some primers and thereafter an efficiency estimation will be provided for each suggested primer pair. Please go back to the <a href="../web.html">form</a>.
        </div> 
        """
        self.cssBottom()
        sys.exit()
