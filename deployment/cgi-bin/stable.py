#!/usr/bin/env python
#primerEfficiency_0.9.8.cgi
#include uri or url escaping

#this avoids zero float division errors
from __future__ import division

import cgi, sys, re, textwrap
from Bio.Emboss.Applications import Primer3Commandline
from Bio.Application import generic_run
from Bio.Emboss import Primer3


def degenerated(query):
    alphabet=['a','c','t','g','A','C','T','G']
    setSequence=set(list(query))
    errorChar= (setSequence-set(alphabet))
    
    if len(errorChar)!=0:
        cssUp()
        print"""
        <div class="articles"> 
        At least one of your sequences (amplicon or primer ones) has nucleotides which are diferent from 'A', 'C', 'T' and 'G'. Unfortunately, this tool doesn't handle 'N' or other degenerated nucleotides. Please go back to the <a href="../web.html">form</a>.
        </div> 
        """
        
        cssBottom()
        sys.exit() 
           
def error():
    cssUp()
    print"""
    <div class="articles"> 
    Please fill at least the 'amplicon sequence' field in the form. This tool will estimate an efficiency value when two primers are provided, or, if both of them aren't, it will design some primers and thereafter an efficiency estimation will be provided for each suggested primer pair. Please go back to the <a href="../web.html">form</a>.
    </div> 
    """
    cssBottom()
    sys.exit()
      
#basic functions    

def complementString(s): 
    """Return the complementary sequence string.""" 
    basecomplement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'} 
    letters = list(s) 
    letters = [basecomplement[base] for base in letters] 
    return ''.join(letters) 

def reverseString(s): 
    """Return the sequence string in reverse order.""" 
    s=s[::-1]
    return s

def reverseComplementString(s): 
    """Return the reverse complement of the dna string.""" 
    s = reverseString(s) 
    s = complementString(s) 
    return s 

def primer3():
    primercl=Primer3Commandline()
    primercl.set_parameter("-sequence", "/tmp/in.pr3")
    primercl.set_parameter("-outfile", "/tmp/out.pr3")
    primercl.set_parameter("-productosize", productosize)
    primercl.set_parameter("-otm", otm)
    primercl.set_parameter("-numreturn", numreturn)
    primercl.set_parameter("-ogcpercent", ogcpercent)
    primercl.set_parameter("-osize", osize)
    primercl.set_parameter("-saltconc", saltconc)
    primercl.set_parameter("-dnaconc", dnaconc)
    primercl.set_parameter("-maxdifftm", maxdifftm)
    primercl.set_parameter("-gcclamp", gcclamp)
    result, r, e = generic_run(primercl)
    
    
def primerParser():
    #To parse primer3 output into a Primer3.Record
    handle = open('/tmp/out.pr3', "r")
    global parsed
    parsed= Primer3.read(handle)
    
    try:
        primer =parsed.primers[0]
        
    except IndexError:
        cssUp()
        print """<div class="articles"> 
        There are not primers satisfying the characteristics you entered. Please <a href="../web.html">try again</a> changing them.
        <br /> 
        </div> 
        """
        cssBottom()
        sys.exit()
    else:
    

        parsedFile= open("/tmp/parsed.pr3","w")
        parsedFile.write("%s,%s" % (primer.forward_seq, primer.reverse_seq))
        parsedFile.close()
        handle.close()
   
def gcCompute(amplicon):
    global gcSequence
    gcSequence =float(amplicon.count("G")+amplicon.count("C"))/ \
    float(amplicon.count("G")+amplicon.count("C")+amplicon.count("A")+ amplicon.count("T"))
    
    global gcForward
    gcForward =float(forward.count("G")+forward.count("C"))/ \
    float(forward.count("G")+forward.count("C")+forward.count("A")+forward.count("T"))
    
    global gcReverse 
    gcReverse=float(reverse.count("G")+reverse.count("C"))/ \
    float(reverse.count("G")+reverse.count("C")+reverse.count("A")+reverse.count("T"))
    
    global gcImbalance
    gcImbalance = abs(gcForward-gcReverse)
    
def triplets():
    forwardTriplets = list()
    for x in range(len(forward)):
        forwardTriplets.append(forward[x:x+3])
    
    reverseTriplets = list()
    for x in range(len(reverse)):
        reverseTriplets.append(reverse[x:x+3])
    
    reverseRevCom = reverseComplementString(reverse)
    forwardRevCom = reverseComplementString(forward)

    forwardRevComTriplets = list()
    for x in range(len(forwardRevCom)):
        forwardRevComTriplets.append(forwardRevCom[x:x+3])
       
    
    reverseRevComTriplets = list()
    for x in range(len(reverseRevCom)):
        reverseRevComTriplets.append(reverseRevCom[x:x+3])
       
    global primerDimers
    primerDimers= int()    
    for item in forwardTriplets:
        if item in reverseRevComTriplets:
            primerDimers+=1
                
    global primersSelfcom
    primersSelfcom= int()    
    for item in forwardTriplets:
        if item in reverseRevComTriplets:
            primerDimers+=1

def primerSelectionHeader():
    print "<h2>Job name %s </h2><br /><br />" %name
    print "Primer pairs were designed according to the data you entered; for each one, efficiency value was estimated.<br/><br/>"

    
def model():
    #this uses lmerFinal<- lmer(efficiency ~ primerDimers + gcImbalance + gcSequence + primersSelfcom + (1 | operator/species/var/template/source), REML=F, data=unido)
    global result
    result= 1.6953225  \
    - 0.0069873 * primerDimers \
    + 0.4949575 * gcImbalance \
    + 0.0134166 *gcSequence \
    - 0.0006811 * primersSelfcom
    
def cleanSequence():
    global sequence
    newLine=re.compile("%0D%0A")
    sequence2=newLine.sub('',sequence)

    others=re.compile("[\s\d\+]")
    sequence=others.sub('',sequence2)
    
    degenerated(sequence)

    handle= open("/tmp/in.pr3", "w")
    handle.write(sequence)
    handle.close()
           
def cssUp():
    print "Content-type: text/html\n"  
    print"""
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
    <html xmlns="http://www.w3.org/1999/xhtml"> 
    <head> 
    <title>PCR efficiency Calculator</title> 
    <meta http-equiv="Content-Language" content="English" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <link rel="stylesheet" type="text/css" href="../style.css" media="screen" /> 
    </head> 
    <body> 
    <div id="wrap"> 
    <div id="header"> 
    <h1><a href="#">PCR efficiency Calculator</a></h1> 
    <h2>Primer- and amplicon-specific PCR estimation tool</h2> 
    </div> 
    <div id="content"> 
    <div class="left">  
    <h2><a href="#">Tool description</a></h2> 
    <div class="articles"> 
    Primer Efficiency is an open source tool based on several software applications (<a href="copyright.html">licensing</a>). <a href="documentation.html">Documentation</a> is available.
    <br /> 
    </div> 
    <h2><a href="#">Efficiency calculator output</a></h2>
    <div class="articles">"""

def cssBottom(): 
    print """
    </div>
    </div>
    <div class="right">  
    <h2>Categories :</h2> 
    <ul> 
    <li><a href="license.html">License</a></li>  
    <li><a href="documentation.html">Documentation</a></li>  
    <li><a href="http://www.upct.es/~genetica/Publicaciones.htm">Lab's main page</a></li>  
    </ul>
    <br />
    <br />
    <img src="../Simbolo_Mono2945.jpg" alt="Escudo" align="right" border="0" height="100" width="100"> 
    </div> 
    <div style="clear: both;"> </div> 
    </div> 
    </div> 
    </div> 
    <div id="footer">

    Copyright 2009 Izaskun Mallona, Genetics, Technical University of Cartagena

    </div>

    </div>
    </body> 
    </html>
    """
         
#cgi-bin input


form = cgi.FieldStorage()
sequence = form.getvalue('sequence') 
forward = form.getvalue('forward') 
reverse = form.getvalue('reverse') 
otm = form.getvalue('otm')
productosize = form.getvalue('productosize')
numreturn = form.getvalue('numreturn')
name= form.getvalue('name')
ogcpercent= form.getvalue('ogcpercent')
osize= form.getvalue('osize')
saltconc= form.getvalue('saltconc')
dnaconc= form.getvalue('dnaconc')
gcclamp= form.getvalue('gcclamp')
maxdifftm= form.getvalue('maxdifftm')

#this calculates efficiency when primer3 is used
if not form.has_key('sequence'):
    error()
    
else:
    cleanSequence()
    
if (not form.has_key('forward') and form.has_key('reverse')) or (form.has_key('forward') and not form.has_key('reverse')):
    cssUp()
    print """<div class="articles"> 
    You have entered one primer sequence only. This tool will estimate an efficiency value when two primers are provided, or, if both of them aren't, it will design some primers and thereafter an efficiency estimation will be provided for each suggested primer pair. Please go back to the <a href="../web.html">form</a>.
    </div> 
    """
    cssBottom()
    sys.exit()
       
if not form.has_key('forward') and not form.has_key('reverse'):

    sequence = sequence.upper()

    out = open("/tmp/in.pr3", "w")
    out.write(sequence)
    out.close()
    
#primer3

    primer3()

    primerParser()

    result=[]
    cssUp()
    primerSelectionHeader()
    
    for i in range(len(parsed.primers)):
        primer = parsed.primers[i]
        amplicon=sequence[primer.forward_start-1: primer.forward_start-1+primer.size]
        forward=primer.forward_seq
        reverse=primer.reverse_seq
        gcImbalance = abs(primer.forward_gc/100-primer.reverse_gc/100) 
        
        #GC computing
        gcCompute(amplicon)
        
        #triplet extraction and cross hybridization computing
        triplets()
        
        #model here
        model()
        print """
        <h2>Primers with expected efficiency %s and length %s</h2>
         <table cellspacing="10 px" cellpadding="20 px">
          <thead>
            <tr>
              <td></td>
              <th>Sequence</th>
              <th>Start</th>
              <th>Length</th>
              <th>Tm</th>
              <th>GC content</th>
            </tr>
          </thead>
         <tbody>
        <tr>
            <td>Forward</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
        </tr>
        <tr>
            <td>Reverse</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
        </tr>
        </tbody>
        </table>
        """ %(result, primer.size, primer.forward_seq, primer.forward_start, primer.forward_length, primer.forward_tm, primer.forward_gc, primer.reverse_seq, primer.reverse_start, primer.reverse_length, primer.reverse_tm, primer.reverse_gc)
        
    cssBottom()
    
else:

    #this is only for efficiency computing
    degenerated(forward)
    degenerated(reverse)
    #uppercase
    forward = forward.upper()
    reverse = reverse.upper()
    sequence= sequence.upper()

    #if primers are not included in the sequence, they should be
    check=0
    forwd = re.compile(forward)
    revse=re.compile(reverse)
    found=0
    
    if forwd.search(sequence) is not None:
        if revse.search(reverseComplementString(sequence)) is not None:
            start=sequence.find(forward)
            end=sequence.find(reverseComplementString(reverse))
            found=1
            
    else:
        if forwd.search(reverseComplementString(sequence)) is not None:
            if revse.search(sequence) is not None:
                start=sequence.find(forward)
                end=sequence.find(reverseComplementString(reverse))
                found=1
        
    if found:
        sequence= sequence[start:end+len(reverse)]
        
    else:
        check=1
        sequence = forward+sequence
        #this is a logical element to advice the absence of primer annealing into the amplicon
        sequence = sequence + reverseComplementString(reverse)

    #GC computing
    gcCompute(sequence)

    #triplet extraction and cross hybridization computing
    triplets()
            
    model()
    cssUp()

    print "The PCR efficiency estimate of your data '%s' is <b>%s</b>" %(name,result)
    if check==1:
        print " <br /><br />Caution, at least one of the primers you provided didn't match with your desired amplicon. In order to compute the efficiency value, the script have concatenated the forward primer to the left side of your sequence, and the reverse complement of the reverse primer to the right side. <br /> <br /> Thus the sequence used as input was: <br /> <br />" 
        print "%s" %textwrap.fill(sequence, width=80)
        print "<br /><br />"

    cssBottom()

