#!/usr/bin/env python
# -*- coding: utf-8  -*-

# This file is part of PCR efficiency calculator.  
# PCR efficiency calculator is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright Izaskun Mallona
# izaskun.mallona@gmail.com

from html import *
# from Bio.Application import generic_run
from Bio.Emboss.Applications import Primer3Commandline
from Bio.Emboss import Primer3

class primer_cl:
    """Primer3 command line application and parser capabilities."""
            
    def primer3(self,productosize,otm,numreturn,ogcpercent ,\
        osize,saltconc,dnaconc,maxdifftm,gcclamp):
        """Calls to primer3 commandline."""
        
        primercl=Primer3Commandline()
        primercl.set_parameter("-sequence", "/tmp/in.pr3")
        primercl.set_parameter("-outfile", "/tmp/out.pr3")
        # primercl.set_parameter("-productosize", productosize)
        primercl.set_parameter("-psizeopt", productosize)
        # primercl.set_parameter("-otm", otm)
        primercl.set_parameter("-otmopt", otm)

        primercl.set_parameter("-numreturn", numreturn)
        primercl.set_parameter("-ogcpercent", ogcpercent)
        primercl.set_parameter("-osize", osize)
        primercl.set_parameter("-saltconc", saltconc)
        primercl.set_parameter("-dnaconc", dnaconc)
        primercl.set_parameter("-maxdifftm", maxdifftm)
        primercl.set_parameter("-gcclamp", gcclamp)

        # https://www.biostars.org/p/17998/
        # result, r, e = generic_run(primercl)
        # result, r, e = primercl
        result = primercl()
           
    def primerParser(self,howMany):
        """
        Parses the primer3 output file into a record.
        
        Returns a list of lists (mother list length is the number
        of primers parsed).
        @return list of lists
            lengthSequence,
            forward start,
            reverse start,
            primers length,
            gcPrimers,
            gcImbalance,
            forward sequence,
            reverse sequence (the two last ones will be piped
            into the primerDimers method)
            
        """
        
        #To parse primer3 output into a Primer3.Record
        handle = open('/tmp/out.pr3', "r")
        parsed= Primer3.read(handle)
        
        try:
            i = 0
            toReturn = []  #nested list
            
            while (i < int(howMany)):
                primer = parsed.primers[i]
#'forward_gc', 'forward_length', 'forward_seq', 'forward_start', 'forward_tm', 'reverse_gc', 'reverse_length', 'reverse_seq', 'reverse_start', 'reverse_tm', 'size'] 
                myTuple = (
                    primer.size,
                    primer.forward_start, #to extract sequence and call gcCompute()
                    primer. reverse_start,
                    primer.forward_length + primer.reverse_length,
                    (primer.forward_gc + primer.reverse_gc)/2,
                    abs(primer.forward_gc - primer.reverse_gc),
                    primer.forward_seq,
                    primer.reverse_seq
                )
                toReturn.append(myTuple)
                i+=1
            return toReturn
##        
##            primer =parsed.primers[0]
            
        except IndexError:
            foo = html()
            foo.cssUp()
            print """<div class="articles"> 
            There are not primers satisfying the characteristics you entered. Please <a href="../web.html">try again</a> changing them.
            <br /> 
            </div> 
            """
            foo.cssBottom()
            sys.exit()
##        else:
##            parsedFile= open("/tmp/parsed.pr3","w")
##            parsedFile.write("%s,%s" % (primer.forward_seq, primer.reverse_seq))
##            parsedFile.close()
            handle.close()

